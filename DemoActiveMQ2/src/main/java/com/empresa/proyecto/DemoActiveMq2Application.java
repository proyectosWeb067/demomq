package com.empresa.proyecto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoActiveMq2Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoActiveMq2Application.class, args);
	}

}
