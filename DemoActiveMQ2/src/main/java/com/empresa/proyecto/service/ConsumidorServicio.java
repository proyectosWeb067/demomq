package com.empresa.proyecto.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.empresa.proyecto.dto.PersonaDTO;

@Component
public class ConsumidorServicio {

	@JmsListener(destination="mensajeCola")
	public void Escuchador(PersonaDTO persona) {
		Log log = LogFactory.getLog(getClass());
		log.info("El nombre de la persona que envio el mensaje es: " + persona.getNombre()+
				" con identificación: " + persona.getCedula());
	}
}
