package com.empresa.proyecto.productor;

import javax.jms.Queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.proyecto.dto.Persona;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("productor")
public class ProductorController {

	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	private Queue mensajeCola;
	
	@PostMapping("/mensaje")
	public Persona enviarMensaje(@RequestBody Persona persona) {
		
		try {
			ObjectMapper maper = new ObjectMapper();
			String  jsonPersona = maper.writeValueAsString(persona);
			
			//convirtiendo jsonPersona
			jmsTemplate.convertAndSend(mensajeCola,jsonPersona); 
			 
		} catch (Exception e) { 
			e.printStackTrace();
		}
		return persona;
	}
}
